package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void successAddingNewEmployee() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String description = "Thunder God";
        String email = "thor@marvel.com";

        Employee newEmployee = new Employee(firstName, lastName, description, email);
        assertNotNull(newEmployee);
    }

    @Test
    void throwsExceptionEmployeeWithWrongFormatEmail() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String description = "Thunder God";
        String email = "thor.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, description, email);
        });

    }

    @Test
    void throwsExceptionDueToEmployeeEmailIncomplete() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String description = "Thunder God";
        String email = "1120717@incomplete";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, description, email);
        });
    }

    @Test
    void throwsExceptionDueToNullFirstName() {

        String lastName = "Marvel";
        String description = "Thunder God";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(null, lastName, description, email);
        });
    }

    @Test
    void throwsExceptionDueToEmptyFirstName() {

        String lastName = "Marvel";
        String description = "Thunder God";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(" ", lastName, description, email);
        });
    }

    @Test
    void throwsExceptionDueToEmptyLastName() {
        String firstName = "Thor";
        String description = "Thunder God";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, "    ", description, email);
        });


    }
    @Test
    void throwsExceptionDueToNullLastName() {
        String firstName = "Thor";
        String description = "Thunder God";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, null, description, email);
        });
    }
    @Test
    void throwsExceptionDueToNullDescription() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, null, email);
        });
    }
    @Test
    void throwsExceptionDueToEmptyDescription() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String email = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, "    ", email);
        });
    }

    @Test
    void throwsExceptionDueToNullEmail() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String description = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, description, null);
        });
    }
    @Test
    void throwsExceptionDueToEmptyEmail() {
        String firstName = "Thor";
        String lastName = "Marvel";
        String description = "thor@marvel.com";

        assertThrows(IllegalArgumentException.class, () -> {
            Employee newEmployee = new Employee(firstName, lastName, description, "");
        });
    }

}